<?php

namespace Carica\Unicode {

  class String implements \Iterator, \ArrayAccess {

    const USE_INTL = 0;
    const USE_ICONV = 1;
    const USE_MBSTRING = 2;

    private $_implementations = array(
      self::USE_INTL => 'Intl',
      self::USE_ICONV => 'Iconv',
      self::USE_MBSTRING => 'Mbstring'
    );

    /**
     * @var null|string Used implemtation/extension wrapper
     */
    private $_implementation = NULL;

    /**
     * @var string the actual string
     */
    private $_content = '';

    /**
     * String length cache
     * @var null|integer
     */
    private $_length = NULL;

    /**
     * Iterator Interface, current position
     * @var integer|NULL
     */
    private $_key = NULL;
    /**
     * Iterator Interface, current character
     * @var string|NULL
     */
    private $_current = NULL;

    public function __construct($string) {
      $this->_content = (string)$string;
    }

    public function __toString() {
      return $this->_content;
    }

    /**
     * Return the character at the given position
     *
     * @param $offset
     * @return string|null
     */
    public function charAt($offset) {
      $char = $this->_substr($this->_content, $offset, 1);
      return ($char != '') ? $char : NULL;
    }

    /**
     * Return the starting offset of $needle or false
     *
     * @param $needle
     * @param int $offset
     * @return false|integer
     */
    public function indexOf($needle, $offset = 0) {
      return $this->_strpos($this->_content, $needle, $offset);
    }

    /**
     * String length (character count), will be cached.
     *
     * @return int|null
     */
    public function length() {
      if (NULL === $this->_length) {
        $this->_length = $this->_strlen($this->_content);
      }
      return $this->_length;
    }

    /**
     * Return a part of the string contents in a new string object
     *
     * @param $start
     * @param null $length
     * @return String
     */
    public function substr($start, $length = NULL) {
      return new self($this->_substr($this->_content, $start, $length));
    }

    /**
     * Set the implementation, returns the used class.
     *
     * @param null|integer|array(integer) $implementation
     * @return null|string
     * @throws \LogicException
     */
    public function implementation($implementation = NULL) {
      if (isset($implementation)) {
        $implementations = is_array($implementation) ? $implementation : array($implementation);
        $this->_implementation = NULL;
      }
      if (NULL === $this->_implementation) {
        if (empty($implementations)) {
          $implementations = array_keys($this->_implementations);
        }
        foreach ($implementations as $implementation) {
          if (!isset($this->_implementations[$implementation])) {
            continue;
          }
          $interface = __NAMESPACE__.'\\String\\Functions';
          $class = __NAMESPACE__.'\\String\\'.$this->_implementations[$implementation];
          if (!interface_exists($interface)) {
            include(__DIR__.'/String/Functions.php');
          }
          if (!class_exists($class)) {
            include(__DIR__.'/String/'.$this->_implementations[$implementation].'.php');
          }
          if (class_exists($class, FALSE) && $class::useable()) {
            return $this->_implementation = $class;
          }
        }
        throw new \LogicException('No usable unicode implementation.');
      }
      return $this->_implementation;
    }

    private function _strlen($string) {
      return call_user_func($this->implementation().'::strlen', $string);
    }

    private function _substr($string, $start, $end = NULL) {
      return call_user_func($this->implementation().'::substr', $string, $start, $end);
    }

    private function _strpos($string, $offset = 0) {
      return call_user_func($this->implementation().'::strpos', $string, $offset);
    }

    /**
     * Iterator Interface: reset position to first character
     */
    public function rewind() {
      $this->_key = -1;
      $this->next();
    }

    /**
     * Iterator Interface: move internal position to next element
     */
    public function next() {
      $this->_current = $this->charAt(++$this->_key);
    }

    /**
     * Iterator Interface: valid character at current position
     *
     * @return string|null
     */
    public function valid() {
      return NULL !== $this->_current;
    }

    /**
     * Iterator Interface: current position
     *
     * @return integer|null
     */
    public function key() {
      return $this->_key;
    }

    /**
     * Iterator Interface: current character
     *
     * @return string|null
     */
    public function current() {
      return $this->_current;
    }

    /**
     * Validate that the given offset exists, or in other words the stirng is longer.
     *
     * @param integer $offset
     * @return bool
     */
    public function offsetExists($offset) {
      return $offset < $this->length();
    }

    /**
     * Returns the character at the given position
     * @param integer $offset
     *
     * @return string
     */
    public function offsetGet($offset) {
      return $this->charAt($offset);
    }

    /**
     * Change the character at the given position
     *
     * @param integer $offset
     * @param integer $value
     *
     * @throws \InvalidArgumentException
     */
    public function offsetSet($offset, $value) {
      if (!$this->offsetExists($offset)) {
        throw new \InvalidArgumentException('Invalid offset: '.$offset);
      }
      $value = (string)$value;
      if ($this->_strlen($value) != 1) {
        throw new \InvalidArgumentException('Not a unicode character: '.$value);
      }
      $this->_content =
        $this->_substr($this->_content, 0, $offset).
        $value.
        $this->_substr($this->_content, $offset + 1);
    }

    /**
     * Unsetting a character index, will remove it from string and shorten the string by one.
     *
     * @param integer $offset
     */
    public function offsetUnset($offset) {
      if ($this->offsetExists($offset)) {
        $this->_content =
          $this->_substr($this->_content, 0, $offset).
          $this->_substr($this->_content, $offset + 1);
        $this->_length = NULL;
      }
    }
  }
}