<?php

namespace Carica\Unicode\String {

  interface Functions {

    static function useable($cached = TRUE);

    static function strlen($string);

    static function substr($string, $start, $length = NULL);

    static function strpos($string, $needle, $offset = 0);

  }

}