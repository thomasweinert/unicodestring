<?php

namespace Carica\Unicode\String {

  class Iconv implements Functions {

    private static $_usable = NULL;

    public static function useable($cached = TRUE) {
      if (NULL === self::$_usable || !$cached) {
        self::$_usable = extension_loaded('iconv');
      }
      return self::$_usable;
    }

    public static function strlen($string) {
      return iconv_strlen($string, 'UTF-8');
    }

    public static function substr($string, $start, $length = NULL) {
      if (NULL === $length) {
        $length = self::strlen($string);
      }
      return iconv_substr($string, $start, $length, 'UTF-8');
    }

    public static function strpos($string, $needle, $offset = 0) {
      return iconv_substr($string, $needle, $offset, 'UTF-8');
    }
  }

}