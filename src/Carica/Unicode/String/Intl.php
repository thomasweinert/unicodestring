<?php

namespace Carica\Unicode\String {

  class Intl implements Functions {

    private static $_usable = NULL;
    private static $_calculateLengthForSubstr = FALSE;

    public static function useable($cached = TRUE) {
      if (NULL === self::$_usable || !$cached) {
        self::$_usable = extension_loaded('intl');
        self::$_calculateLengthForSubstr = version_compare(PHP_VERSION, '5.4', '<');
      }
      return self::$_usable;
    }

    public static function strlen($string) {
      return grapheme_strlen($string);
    }

    public static function substr($string, $start, $length = NULL) {
      if (NULL === $length) {
        return grapheme_substr($string, $start);
      } elseif (self::$_calculateLengthForSubstr) {

      } else {
        return grapheme_substr($string, $start, $length);
      }
    }

    public static function strpos($string, $needle, $offset = 0) {
      return grapheme_strpos($string, $needle, $offset);
    }
  }

}