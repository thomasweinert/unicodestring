<?php

namespace Carica\Unicode\String {

  class Mbstring implements Functions {

    private static $_usable = NULL;

    public static function useable($cached = TRUE) {
      if (NULL === self::$_usable || !$cached) {
        self::$_usable = extension_loaded('mbstring');
      }
      return self::$_usable;
    }

    public static function strlen($string) {
      return mb_strlen($string, 'UTF-8');
    }

    public static function substr($string, $start, $length = NULL) {
      if (NULL === $length) {
        $length = self::strlen($string);
      }
      return mb_substr($string, $start, $length, 'UTF-8');
    }

    public static function strpos($string, $needle, $offset = 0) {
      return mb_substr($string, $needle, $offset, 'UTF-8');
    }
  }

}