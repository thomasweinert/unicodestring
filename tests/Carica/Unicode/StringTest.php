<?php

namespace Carica\Unicode {

  require_once(__DIR__.'/../../../src/Carica/Unicode/String.php');

  /**
   * @covers Carica\Unicode\String
   */
  class StringTest extends \PHPUnit_Framework_Testcase {

    public function testConstructor() {
      $string = new String('ÄÖÜ');
      $this->assertEquals('ÄÖÜ', (string)$string);
    }

    /**
     * @dataProvider provideImplementations
     * @param integer $implementation
     */
    public function testIterator($implementation) {
      $string = new String('ÄÖÜ');
      $string->implementation($implementation);
      $this->assertEquals(array('Ä', 'Ö', 'Ü'), iterator_to_array($string));
    }

    public function testCharAt() {
      $string = new String('ÄÖÜ');
      $this->assertEquals('Ü', $string->charAt(2));
    }

    /**
     * @dataProvider provideImplementations
     * @param integer $implementation
     */
    public function testLength($implementation) {
      $string = new String('ÄÖÜ');
      $string->implementation($implementation);
      $this->assertEquals(3, $string->length());
    }

    /**
     * @dataProvider provideImplementations
     * @param integer $implementation
     */
    public function testIndexOfExpectingOne() {
      $string = new String('ÄÖÜ');
      $this->assertEquals(1, $string->indexOf('ÖÜ'));
    }

    /**
     * @dataProvider provideImplementations
     * @param integer $implementation
     */
    public function testIndexOfNotFoundExpectingFalse() {
      $string = new String('ÄÖÜ');
      $this->assertFalse($string->indexOf('ABC'));
    }

    public function testSubStr() {
      $string = new String('ÄÖÜ');
      $part = $string->substr(1, 50);
      $this->assertInstanceOf(__NAMESPACE__.'\\String', $part);
      $this->assertEquals('ÖÜ', (string)$part);
    }

    public static function provideImplementations() {
      $implementations = array();
      if (extension_loaded('intl')) {
        $implementations['intl'] = array(String::USE_INTL);
      }
      if (extension_loaded('iconv')) {
        $implementations['iconv'] = array(String::USE_ICONV);
      }
      if (extension_loaded('mbstring')) {
        $implementations['mbstring'] = array(String::USE_MBSTRING);
      }
      return $implementations;
    }
  }

}